﻿using System;

namespace program1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter values");

            double x = Convert.ToDouble(Console.ReadLine());
            double y = Convert.ToDouble(Console.ReadLine());
            double z = Convert.ToDouble(Console.ReadLine());

            double A = 1 / 11 + 3 / Math.Exp(Math.Abs(x));
            double B = x / 3 + Math.Min(x, 2 * x);
            double C = 3 * x * Math.Sin(1 / 3) + Math.Pow(x, (x - 1) / 2);
            double f = A + B + C;

            Console.WriteLine($"f({x}) = {f}");


        }
    }
}
